Friday Lunch
============

:date: 2015-12-11
:author: John C.

:event-start: 2015-12-11 12:30
:event-duration: 1h 3Om
:location: Baja Burrito
:summary: Food

:pin: 35.779571, -78.675519

Exchange gifts, and eat.