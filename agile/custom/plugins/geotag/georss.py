from __future__ import unicode_literals

from feedgenerator import RssFeed, rfc2822_date


class GeoRssFeed(RssFeed):

    def rss_attributes(self):

        return {'version': '2.0',
                'xmlns:atom': "http://www.w3.org/2005/Atom",
                'xmlns:georss': "http://www.georss.org/georss",
                'xmlns:gml': "http://www.opengis.net/gml",
                }

    def add_item_elements(self, handler, item):

        handler.addQuickElement("title", item['title'])
        handler.addQuickElement("link", item['link'])
        if item['description'] is not None:
            handler.addQuickElement("description", item['description'])

        # Author information.
        if item["author_name"] and item["author_email"]:
            handler.addQuickElement("author", "%s (%s)" % \
                (item['author_email'], item['author_name']))
        elif item["author_email"]:
            handler.addQuickElement("author", item["author_email"])
        elif item["author_name"]:
            handler.addQuickElement("dc:creator", item["author_name"], {"xmlns:dc": "http://purl.org/dc/elements/1.1/"})

        if item['pubdate'] is not None:
            handler.addQuickElement("pubDate", rfc2822_date(item['pubdate']))
        if item['comments'] is not None:
            handler.addQuickElement("comments", item['comments'])
        if item['unique_id'] is not None:
            handler.addQuickElement("guid", item['unique_id'], attrs={'isPermaLink': 'false'})
        if item['ttl'] is not None:
            handler.addQuickElement("ttl", item['ttl'])

        # Enclosure.
        if item['enclosure'] is not None:
            handler.addQuickElement("enclosure", '',
                {"url": item['enclosure'].url, "length": item['enclosure'].length,
                    "type": item['enclosure'].mime_type})

        # Categories.
        for cat in item['categories']:
            handler.addQuickElement("category", cat)

        # GeoRSS
        if item['pin'] is not None:

            #handler.addQuickElement('georss:point', item['pin'])

            handler.startElement('georss:where', {})
            handler.startElement('gml:Point', {})
            handler.startElement('gml:pos', {})
            handler.characters(item['pin'])
            handler.endElement('gml:pos')
            handler.endElement('gml:Point')
            handler.endElement('georss:where')

        #if item['georss_line'] is not None:
        #    handler.addQuickElement('georss:line', item['georss_line'])
        #if item['georss_polygon'] is not None:
        #    handler.addQuickElement('georss:polygon', item['georss_polygon'])
        #if item['georss_box'] is not None:
        #    handler.addQuickElement('georss:box', item['georss_box'])

