#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = u'Southeast Regional Climate Hub'
SITENAME = u'AGriculture InterLinks to Extension'
SITEURL = 'http://jgcobb.bitbucket.org'

THEME = 'themes/foundation-default-colours'
#THEME = 'themes/basic'

PATH = 'content'

OUTPUT_PATH = '../'
DELETE_OUTPUT_DIRECTORY = False


TIMEZONE = 'America/New_York'

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_RSS = 'feeds/rss.xml'
FEED_ALL_ATOM = None
FEED_GEOTAG = True

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True


PLUGIN_PATHS = ['custom/plugins/', 'plugins/']
PLUGINS = ['geotag', ] # 'events'


PLUGIN_EVENTS = {
    'ics_fname': '../calendar.ics',
}

# EMBED JAVASCRIPT SEE:
#http://www.whatrocks.org/adding-js-to-pelican-p1.html

STATIC_PATHS = ['html']


